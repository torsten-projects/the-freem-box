import os


class Configuration:
    def __init__(self) -> None:
        self.spotify_client = self._get_string("SPOTIFY_API_CLIENT_ID")
        self.spotify_secret = self._get_string("SPOTIFY_API_KEY")
        self.youtube_api_keys = self._get_string("YOUTUBE_API_KEYS").split(";")

    def _get_string(
        self, name: str, default_value: str = None, can_be_none: bool = False
    ) -> str:
        missing = False
        env_value = os.getenv(name)
        if env_value is None or len(env_value) == 0:
            if default_value is None:
                missing = True
            else:
                return default_value
        else:
            return env_value
        if (missing or env_value is None) and not can_be_none:
            raise Exception(f"Missing environment variable '{name}'")
        else:
            return None

    def _get_int(self, name: str) -> int:
        return int(self._get_string(name))
