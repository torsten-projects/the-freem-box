import logging
import os

from fastapi import FastAPI, HTTPException, Query, Response
from fastapi.responses import FileResponse, RedirectResponse

from backend.analysis import AnalysisEngine
from backend.configuration import Configuration
import backend.types.legacy
import backend.types.api_v1


class Backend:
    def __init__(self) -> None:
        self._configuration = Configuration()
        self._logger = logging.getLogger("backend")
        self._analysis_engine = AnalysisEngine(self._configuration)

        self.app = FastAPI(
            title="The Freem Jukebox API",
            description="API for analysis and downloading songs from Spotify and YouTube",
            version="1.1.0",
        )

        self._prepare_routes_system()
        self._prepare_routes_legacy()
        self._prepare_routes_api_v1()
        self._prepare_routes_frontend()

    def _prepare_routes_frontend(self) -> None:
        @self.app.get("/", response_class=RedirectResponse, tags=["Frontend"])
        async def serve_index_redirect():
            return RedirectResponse(url="/jukebox_index.html")

        @self.app.get("/{path:path}", tags=["Frontend"])
        async def serve_static(path: str):
            full_lookup_path = f"web/{path}"
            if not os.path.isfile(full_lookup_path):
                self._logger.warning(f"Unable to serve {full_lookup_path}!")
                raise HTTPException(status_code=404)

            return FileResponse(full_lookup_path, media_type=self._get_mime_type(path))

    def _prepare_routes_system(self) -> None:
        @self.app.get("/ready", tags=["System"])
        async def serve_ready():
            return Response(status_code=200)

    def _prepare_routes_legacy(self) -> None:
        @self.app.post("/api/site/shrink", tags=["Legacy"])
        async def serve_shrink(request: backend.types.legacy.ShrinkRequest) -> dict:
            self._logger.debug("Shrink called")
            return request.form_data

        @self.app.get(
            "/api/analysis/analyse",
            response_model=backend.types.legacy.AnalysisResponse,
            tags=["Legacy"],
        )
        async def serve_analysis_spotify(
            spotify_id: str = Query(..., min_length=5, max_length=40),
        ):
            if len(spotify_id) > 40 or len(spotify_id) < 5:
                raise HTTPException(status_code=400, detail="Invalid spotify_id")

            youtube_link, song_title = await self._translate_spotify_id_to_youtube(
                spotify_id
            )
            analysis = await self._get_analysis_object_for_youtube_url(
                youtube_link, song_title
            )

            self._logger.info(
                f"Returning analysis for Spotify query {song_title=} {spotify_id=}"
            )
            return analysis

        @self.app.get(
            "/api/analysis/analyse-youtube",
            response_model=backend.types.legacy.AnalysisResponse,
            tags=["Legacy"],
        )
        async def serve_analysis_youtube(
            youtube_url: str = Query(..., min_length=5, max_length=120),
            title: str = Query(default="Unnamed"),
        ):
            if len(youtube_url) > 120 or len(youtube_url) < 5:
                raise HTTPException(status_code=400, detail="Invalid youtube_url")

            analysis = await self._get_analysis_object_for_youtube_url(
                youtube_url, title
            )

            self._logger.info(
                f"Returning analysis for Youtube query {youtube_url=} {title=}"
            )
            return analysis

        @self.app.get("/api/audio/youtube", tags=["Legacy"])
        async def serve_audio_youtube(youtube_url: str = Query(...)):
            (
                audio_bytes,
                _,
            ) = await self._analysis_engine.get_audio_data_for_youtube_link(youtube_url)
            if len(audio_bytes) == 0:
                raise HTTPException(status_code=500, detail="Downloaded audio is empty")
            return Response(content=audio_bytes, media_type="audio/aac")

        @self.app.get(
            "/api/analysis/search",
            response_model=list[backend.types.legacy.SearchResult],
            tags=["Legacy"],
        )
        async def serve_search_spotify(
            query: str = Query(..., max_length=100),
            limit_count: int = Query(default=30, ge=1, le=50),
        ):
            self._logger.info(f"Searching spotify for {query=}")
            raw_results = (
                await self._analysis_engine.get_spotify_results_for_query(
                    query, limit_count
                )
            )["tracks"]["items"]

            results = [
                backend.types.legacy.SearchResult(
                    service="SPOTIFY",
                    id=result["id"],
                    name=result["name"],
                    title=result["name"],
                    artist=", ".join([artist["name"] for artist in result["artists"]]),
                    duration=result["duration_ms"],
                )
                for result in raw_results
            ]

            self._logger.info(f'Returning {len(results)} results for "{query}"')
            return results

        @self.app.get(
            "/api/analysis/search-youtube",
            response_model=list[backend.types.legacy.SearchResult],
            tags=["Legacy"],
        )
        async def serve_search_youtube(
            query: str = Query(..., max_length=100),
            limit_count: int = Query(default=30, ge=1, le=50),
        ):
            self._logger.info(f"Searching YouTube for {query=}")
            raw_results = self._analysis_engine.get_youtube_hits_for_query(
                query, limit_count
            )

            results = [
                backend.types.legacy.SearchResult(
                    service="YOUTUBE",
                    id=result.id,
                    name=result.title,
                    title=result.title,
                    artist=None,
                    duration=result.length,
                )
                for result in raw_results
            ]

            self._logger.info(f'Returning {len(results)} results for "{query}"')
            return results

    def _prepare_routes_api_v1(self) -> None:
        @self.app.get(
            "/api/v1/search",
            tags=["API V1"],
            response_model=list[backend.types.api_v1.SearchResult],
        )
        async def search(
            service: backend.types.api_v1.SearchProvider = Query(
                ..., description="Search service to query"
            ),
            search_term: str = Query(
                ..., min_length=1, max_length=200, description="The query to search for"
            ),
            result_limit: int = Query(
                default=20,
                ge=1,
                le=50,
                description="How many results to return (at most, real amount returned may be lower or zero)",
            ),
        ):
            if service == backend.types.api_v1.SearchProvider.YOUTUBE:
                # Handle YouTube search
                raw_results = self._analysis_engine.get_youtube_hits_for_query(
                    search_term, result_limit
                )
                results = [
                    backend.types.api_v1.SearchResult(
                        name=result.title,
                        identifier=result.id,
                        length=result.length,
                    )
                    for result in raw_results
                ]
            elif service == backend.types.api_v1.SearchProvider.SPOTIFY:
                # Handle Spotify search
                self._logger.info(f"Searching spotify for {search_term=}")
                raw_results = (
                    await self._analysis_engine.get_spotify_results_for_query(
                        search_term, result_limit
                    )
                )["tracks"]["items"]
                results = [
                    backend.types.api_v1.SearchResult(
                        name=result["name"]
                        + " - "
                        + ", ".join([artist["name"] for artist in result["artists"]]),
                        identifier=result["id"],
                        length=result["duration_ms"] / 1000,
                    )
                    for result in raw_results
                ]
            else:
                raise HTTPException(status_code=400, detail="Invalid search provider")

            return results

        @self.app.get(
            "/api/v1/analyze",
            tags=["API V1"],
            response_model=backend.types.api_v1.AnalysisResult,
        )
        async def analyze(
            audio_provider: backend.types.api_v1.AnalysisAudioProvider = Query(
                ..., description="Type of the service which provides audio input"
            ),
            identifier: str = Query(..., description="Identifier of the audio input"),
        ):
            if audio_provider == backend.types.api_v1.AnalysisAudioProvider.SPOTIFY:
                # For Spotify inputs, get the corresponding YouTube link
                try:
                    youtube_link, _ = await self._translate_spotify_id_to_youtube(
                        identifier
                    )
                except Exception as e:
                    self._logger.error(
                        f"Error translating Spotify ID {identifier}: {str(e)}"
                    )
                    raise HTTPException(status_code=500, detail=str(e))
            elif audio_provider == backend.types.api_v1.AnalysisAudioProvider.YOUTUBE:
                # Direct YouTube input, use it for analysis
                youtube_link = f"https://www.youtube.com/watch?v={identifier}"
            else:
                raise HTTPException(status_code=400, detail="Invalid audio provider")

            (
                youtube_data,
                youtube_filename,
            ) = await self._analysis_engine.get_audio_data_for_youtube_link(
                youtube_link
            )
            if len(youtube_data) == 0:
                raise HTTPException(status_code=500, detail="Downloaded audio is empty")

            analysis = await self._analysis_engine.get_analysis_for_youtube_audio(
                youtube_filename
            )

            resp = {
                "info": {
                    "service": audio_provider,
                    "identifier": identifier,
                    "length": analysis["track"]["duration"],
                },
                "analysis": {
                    "sections": [
                        backend.types.api_v1.Section(
                            start=s["start"],
                            duration=s["duration"],
                            confidence=s["confidence"],
                        )
                        for s in analysis["sections"]
                    ],
                    "bars": [
                        backend.types.api_v1.Bar(
                            start=b["start"],
                            duration=b["duration"],
                            confidence=b["confidence"],
                        )
                        for b in analysis["bars"]
                    ],
                    "beats": [
                        backend.types.api_v1.Beat(
                            start=bt["start"],
                            duration=bt["duration"],
                            confidence=bt["confidence"],
                        )
                        for bt in analysis["beats"]
                    ],
                    "tatums": [
                        backend.types.api_v1.Tatum(
                            start=t["start"],
                            duration=t["duration"],
                            confidence=t["confidence"],
                        )
                        for t in analysis["tatums"]
                    ],
                    "segments": [
                        backend.types.api_v1.Segment(
                            start=seg["start"],
                            duration=seg["duration"],
                            confidence=seg["confidence"],
                            loudness_start=seg["loudness_start"],
                            loudness_max=seg["loudness_max"],
                            loudness_max_time=seg["loudness_max_time"],
                            loudness_end=seg["loudness_end"],
                            pitches=seg["pitches"],
                            timbre=seg["timbre"],
                        )
                        for seg in analysis["segments"]
                    ],
                },
            }

            return resp

        @self.app.get("/api/v1/audio", tags=["API V1"], response_model=bytes)
        async def get_audio(
            audio_provider: backend.types.api_v1.DownloadAudioProvider = Query(
                ..., description="Type of the service from which to download the audio"
            ),
            identifier: str = Query(..., description="Identifier of the audio input"),
        ):
            if audio_provider == backend.types.api_v1.AnalysisAudioProvider.SPOTIFY:
                # For Spotify inputs, get the corresponding YouTube link
                try:
                    youtube_link, _ = await self._translate_spotify_id_to_youtube(
                        identifier
                    )
                except Exception as e:
                    self._logger.error(
                        f"Error translating Spotify ID {identifier}: {str(e)}"
                    )
                    raise HTTPException(status_code=500, detail=str(e))
            elif audio_provider == backend.types.api_v1.AnalysisAudioProvider.YOUTUBE:
                # Direct YouTube input, use it for analysis
                youtube_link = f"https://www.youtube.com/watch?v={identifier}"
            else:
                raise HTTPException(status_code=400, detail="Invalid audio provider")

            (
                youtube_data,
                _,
            ) = await self._analysis_engine.get_audio_data_for_youtube_link(
                youtube_link
            )
            if len(youtube_data) == 0:
                raise HTTPException(status_code=500, detail="Downloaded audio is empty")

            return Response(content=youtube_data, media_type="audio/aac")

    def _get_mime_type(self, path: str) -> str:
        if path.endswith(".html"):
            return "text/html"
        elif path.endswith(".js"):
            return "text/javascript"
        elif path.endswith(".css"):
            return "text/css"
        return "text/plain"

    async def _get_analysis_object_for_youtube_url(
        self, youtube_url: str, title: str
    ) -> dict:
        (
            youtube_data,
            youtube_filename,
        ) = await self._analysis_engine.get_audio_data_for_youtube_link(youtube_url)
        if len(youtube_data) == 0:
            raise HTTPException(status_code=500, detail="Downloaded audio is empty")

        analysis = await self._analysis_engine.get_analysis_for_youtube_audio(
            youtube_filename
        )

        track_duration = analysis["track"]["duration"]
        return {
            "info": {
                "service": "ESSENTIA",
                "name": title,
                "url": youtube_url,
                "duration": track_duration,
            },
            "analysis": {
                "sections": analysis["sections"],
                "bars": analysis["bars"],
                "beats": analysis["beats"],
                "tatums": analysis["tatums"],
                "segments": analysis["segments"],
            },
            "audio_summary": {"duration": track_duration},
        }

    async def _translate_spotify_id_to_youtube(self, spotify_id: str):
        self._logger.info(f"Fetching track info for {spotify_id=}")
        track_info = await self._analysis_engine.get_track_info_for_spotify_id(
            spotify_id
        )
        song_title = track_info["name"]
        artist = track_info["artists"][0]["name"]
        song_duration = track_info["duration_ms"] / 1000
        search_query = f"{artist} - {song_title}"
        self._logger.info(
            f'Getting youtube link for "{artist} - {song_title}" matching {song_duration}s'
        )
        youtube_link = await self._analysis_engine.get_youtube_link(
            search_query, song_duration
        )

        self._logger.info(
            f"Spotify ID {spotify_id=} translated to Youtube URL {youtube_link=} by query '{search_query}'"
        )
        return youtube_link, search_query
