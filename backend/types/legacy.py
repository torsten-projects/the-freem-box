from pydantic import BaseModel


class ShrinkRequest(BaseModel):
    form_data: dict


class SearchResult(BaseModel):
    service: str
    id: str
    name: str
    title: str
    artist: str | None
    duration: int


class AnalysisSection(BaseModel):
    # TODO fill in
    pass


class Analysis(BaseModel):
    # TODO document the classes
    sections: list[AnalysisSection]
    bars: list[dict]
    beats: list[dict]
    tatums: list[dict]
    segments: list[dict]


class AudioSummary(BaseModel):
    duration: float


class AnalysisResponse(BaseModel):
    info: dict
    analysis: Analysis
    audio_summary: AudioSummary
