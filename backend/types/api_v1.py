import enum

from pydantic import BaseModel, Field


class SearchProvider(str, enum.Enum):
    YOUTUBE = "youtube"
    SPOTIFY = "spotify"


class SearchResult(BaseModel):
    name: str = Field(
        ...,
        description="Name of the result - can be a video title or a song name alongside the author",
    )
    identifier: str = Field(
        ...,
        description="Unique identifier of the result - can be a song ID, or a video ID",
    )
    length: float = Field(..., description="Length of the result in seconds")


class AnalysisAudioProvider(str, enum.Enum):
    YOUTUBE = "youtube"
    SPOTIFY = "spotify"


class TrackInfo(BaseModel):
    service: AnalysisAudioProvider = Field(
        ..., description="The audio analysis service used (either YouTube or Spotify)"
    )
    identifier: str = Field(
        ..., description="Unique identifier for the track, like a song ID or video ID"
    )
    length: float = Field(..., description="Length of the track in seconds")


class Section(BaseModel):
    start: float = Field(..., description="Start time of the section in seconds")
    duration: float = Field(..., description="Duration of the section in seconds")
    confidence: float = Field(
        ...,
        description="Confidence score indicating how certain the model is about this section",
    )


class Bar(BaseModel):
    start: float = Field(..., description="Start time of the bar in seconds")
    duration: float = Field(..., description="Duration of the bar in seconds")
    confidence: float = Field(
        ...,
        description="Confidence score indicating how certain the model is about this bar",
    )


class Beat(BaseModel):
    start: float = Field(..., description="Start time of the beat in seconds")
    duration: float = Field(..., description="Duration of the beat in seconds")
    confidence: float = Field(
        ...,
        description="Confidence score indicating how certain the model is about this beat",
    )


class Tatum(BaseModel):
    start: float = Field(..., description="Start time of the tatum in seconds")
    duration: float = Field(..., description="Duration of the tatum in seconds")
    confidence: float = Field(
        ...,
        description="Confidence score indicating how certain the model is about this tatum",
    )


class Segment(BaseModel):
    start: float = Field(..., description="Start time of the segment in seconds")
    duration: float = Field(..., description="Duration of the segment in seconds")
    confidence: float = Field(
        ...,
        description="Confidence score indicating how certain the model is about this segment",
    )
    loudness_start: float = Field(
        ..., description="Loudness at the start of the segment"
    )
    loudness_max: float = Field(..., description="Maximum loudness within the segment")
    loudness_max_time: float = Field(
        ..., description="Time at which maximum loudness occurs within the segment"
    )
    loudness_end: float = Field(..., description="Loudness at the end of the segment")
    pitches: list[float] = Field(..., description="List of pitch values in the segment")
    timbre: list[float] = Field(..., description="List of timbre values in the segment")


class Analysis(BaseModel):
    sections: list[Section] = Field(
        ..., description="List of musical sections in the track"
    )
    bars: list[Bar] = Field(..., description="List of musical bars in the track")
    beats: list[Beat] = Field(..., description="List of musical beats in the track")
    tatums: list[Tatum] = Field(..., description="List of musical tatums in the track")
    segments: list[Segment] = Field(
        ..., description="List of musical segments in the track"
    )


class AnalysisResult(BaseModel):
    info: TrackInfo = Field(
        ...,
        description="Track information including service, name, identifier and length",
    )
    analysis: Analysis = Field(
        ..., description="Detailed musical and audio analysis of the track"
    )


class DownloadAudioProvider(str, enum.Enum):
    YOUTUBE = "youtube"
    SPOTIFY = "spotify"
