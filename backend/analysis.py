from dataclasses import dataclass
from io import BytesIO
import logging
from typing import Dict, Optional
import datetime
import hashlib
import os
import pickle
import typing
import googleapiclient.discovery
import metomi.isodatetime.parsers
import random
import essentia
import essentia.standard

import aiohttp
import aiohttp.helpers
import numpy
import pydub
import yt_dlp

from backend.configuration import Configuration


@dataclass
class YoutubeSearchHit:
    id: str
    length: float
    title: str


class AnalysisEngine:
    def __init__(self, configuration: Configuration):
        self._configuration = configuration
        self._bearer_token: Optional[str] = None
        self._bearer_token_creation_date: datetime.datetime = datetime.datetime.now()
        self._logger = logging.getLogger("analysis-engine")
        self._current_api_key_position = random.randint(
            0, len(self._configuration.youtube_api_keys) - 1
        )

    async def get_analysis_for_youtube_audio(self, audio_file_path: str) -> Dict | None:
        self._logger.info(f"Starting analysis of {audio_file_path=}")

        url_hash = hashlib.sha256(audio_file_path.encode("utf-8")).hexdigest()
        cache_directory = "cache/analysis"
        if not os.path.isdir(cache_directory):
            os.makedirs(cache_directory)
        target_filename = f"{cache_directory}/{url_hash}.pickle"
        if os.path.isfile(target_filename):
            self._logger.info(f"Cache hit on audio analysis {audio_file_path=}")
            with open(target_filename, "rb") as f:
                return pickle.load(f)
        self._logger.info(f"Cache miss on audio analysis {audio_file_path=}")

        loader = essentia.standard.MonoLoader(filename=audio_file_path)
        audio: numpy.ndarray = loader()

        if len(audio.shape) != 1:
            self._logger.error("Malformed audio array")
            return None

        rhythm_extractor = essentia.standard.RhythmExtractor2013()
        key_extractor = essentia.standard.KeyExtractor()
        essentia.standard.Loudness()

        bpm, beats_pos, beats_conf, _, beats_intervals = rhythm_extractor(audio)
        key, scale, key_strength = key_extractor(audio)

        frame_size = 2048
        hop_size = 1024
        w = essentia.standard.Windowing(type="hann")
        spectrum = essentia.standard.Spectrum()
        mfcc = essentia.standard.MFCC()
        pitch_extractor = essentia.standard.PitchYinFFT()

        results = {
            "track": {"duration": float(len(audio) / loader.paramValue("sampleRate"))},
            "bars": [],
            "beats": [],
            "sections": [],
            "segments": [],
            "tatums": [],
        }

        # Process beats into bars (assuming 4/4 time signature)
        # TODO check time signature matches
        for i in range(0, len(beats_pos) - 1):
            beat_data = {
                "start": float(beats_pos[i]),
                "duration": float(beats_pos[i + 1] - beats_pos[i]),
                "confidence": float(beats_conf),
            }
            results["beats"].append(beat_data)

            # Group every 4 beats into a bar
            if i % 4 == 0 and i + 4 <= len(beats_pos):
                bar_duration = (
                    float(beats_pos[i + 3] - beats_pos[i])
                    if i + 4 < len(beats_pos)
                    else float(beats_pos[-1] - beats_pos[i])
                )
                bar_data = {
                    "start": float(beats_pos[i]),
                    "duration": bar_duration,
                    "confidence": float(beats_conf),
                }
                results["bars"].append(bar_data)

        # TODO do we need sections? we cannot easily extract them
        section_length = (
            len(audio) / loader.paramValue("sampleRate") / 4
        )  # divide into 4 sections
        for i in range(4):
            section_data = {
                "start": float(i * section_length),
                "duration": float(section_length),
                "confidence": 0.0,
            }
            results["sections"].append(section_data)

        # Process frames for segments
        for frame_idx, frame in enumerate(
            essentia.standard.FrameGenerator(
                audio, frameSize=frame_size, hopSize=hop_size
            )
        ):
            spectrum_frame = spectrum(w(frame))
            mfcc_bands, mfcc_coeffs = mfcc(spectrum_frame)
            pitch, pitch_conf = pitch_extractor(spectrum_frame)

            # Calculate frame timing
            start_time = frame_idx * hop_size / loader.paramValue("sampleRate")
            duration = frame_size / loader.paramValue("sampleRate")

            # Generate normalized pitch vector (12 values for each pitch class)
            pitch_vector = numpy.zeros(12)
            if pitch > 0:
                pitch_class = int(round(pitch) % 12)
                pitch_vector[pitch_class] = pitch_conf

            segment_data = {
                "start": float(start_time),
                "duration": float(duration),
                "confidence": float(pitch_conf),
                "loudness_start": float(numpy.mean(mfcc_bands)),
                "loudness_max": float(numpy.max(mfcc_bands)),
                "loudness_max_time": float(duration / 2),  # simplified
                "loudness_end": float(numpy.mean(mfcc_bands)),
                "pitches": [float(p) for p in pitch_vector],
                "timbre": [
                    float(t) for t in mfcc_coeffs[:12]
                ],  # Use MFCC coefficients for timbre
            }
            results["segments"].append(segment_data)

        # Generate tatums (subdivisions of beats)
        # TODO how many tatums per beat?
        tatum_interval = 60.0 / (bpm * 2)  # assuming 2 tatums per beat
        num_tatums = int(results["track"]["duration"] / tatum_interval)
        for i in range(num_tatums):
            tatum_data = {
                "start": float(i * tatum_interval),
                "duration": float(tatum_interval),
                "confidence": float(beats_conf),
            }
            results["tatums"].append(tatum_data)

        self._logger.info(f"Completed analysis of {audio_file_path}")

        with open(target_filename, "wb") as f:
            pickle.dump(results, f)

        return results

    async def get_analysis_for_spotify_id(self, spotify_id: str) -> Dict:
        id_hash = hashlib.sha256(spotify_id.encode("utf-8")).hexdigest()
        cache_directory = "cache/analysis"
        if not os.path.isdir(cache_directory):
            os.makedirs(cache_directory)
        target_filename = f"{cache_directory}/{id_hash}.pickle"
        if not os.path.isfile(target_filename):
            print(f"Getting analysis for {spotify_id} for the first time")
            await self._check_spotify_token()
            target_url = f"https://api.spotify.com/v1/audio-analysis/{spotify_id}"  # TODO add spotify ID format validation
            async with aiohttp.ClientSession() as session:
                async with session.get(
                    target_url,
                    headers={"Authorization": f"Bearer {self._bearer_token}"},
                ) as response:
                    result = await response.json()
                    with open(target_filename, "wb") as f:
                        pickle.dump(result, f)
        else:
            print(f"Getting analysis for {spotify_id} from cache")
        with open(target_filename, "rb") as f:
            return pickle.load(f)

    async def get_track_info_for_spotify_id(self, spotify_id: str) -> Dict:
        id_hash = hashlib.sha256(spotify_id.encode("utf-8")).hexdigest()
        cache_directory = "cache/track"
        if not os.path.isdir(cache_directory):
            os.makedirs(cache_directory)
        target_filename = f"{cache_directory}/{id_hash}.pickle"
        if not os.path.isfile(target_filename):
            print(f"Getting track info for {spotify_id} for the first time")
            await self._check_spotify_token()
            target_url = f"https://api.spotify.com/v1/tracks/{spotify_id}"
            async with aiohttp.ClientSession() as session:
                async with session.get(
                    target_url,
                    headers={"Authorization": f"Bearer {self._bearer_token}"},
                ) as response:
                    result = await response.json()
                    with open(target_filename, "wb") as f:
                        pickle.dump(result, f)
        else:
            print(f"Getting track info for {spotify_id} from cache")
        with open(target_filename, "rb") as f:
            return pickle.load(f)

    async def get_spotify_results_for_query(self, query: str, limit: int) -> Dict:
        await self._check_spotify_token()
        target_url = "https://api.spotify.com/v1/search"
        async with aiohttp.ClientSession() as session:
            async with session.get(
                target_url,
                params={"q": query, "type": "track", "limit": limit},
                headers={"Authorization": f"Bearer {self._bearer_token}"},
            ) as response:
                result = await response.json()
                return result

    async def _check_spotify_token(self) -> None:
        if self._bearer_token is None or (
            typing.cast(
                datetime.timedelta,
                datetime.datetime.now() - self._bearer_token_creation_date,
            ).total_seconds()
            > 300
        ):
            self._bearer_token = await self._get_spotify_bearer_token()
            self._bearer_token_creation_date = datetime.datetime.now()

    async def _get_spotify_bearer_token(
        self,
    ) -> str:  # TODO add bearer token refresh on failed request
        target_url = "https://accounts.spotify.com/api/token"
        async with aiohttp.ClientSession(
            auth=aiohttp.helpers.BasicAuth(
                self._configuration.spotify_client, self._configuration.spotify_secret
            )
        ) as session:
            async with session.post(
                target_url,
                data={"grant_type": "client_credentials"},
                headers={"Content-Type": "application/x-www-form-urlencoded"},
            ) as response:
                result = await response.json()
                try:
                    bearer_token: str = result["access_token"]
                    return bearer_token
                except KeyError as e:
                    print(
                        f"Invalid response from Spotfy bearer token provisioner ({e}): {result}"
                    )
                    raise

    def get_youtube_hits_for_query(
        self, name: str, max_results: int = 10
    ) -> list[YoutubeSearchHit]:
        current_api_key = self._configuration.youtube_api_keys[
            self._current_api_key_position
        ]
        self._current_api_key_position += 1
        self._current_api_key_position %= len(self._configuration.youtube_api_keys)

        youtube_svc = googleapiclient.discovery.build(
            "youtube", "v3", developerKey=current_api_key
        )
        youtube_request = youtube_svc.search().list(
            part="snippet",
            maxResults=max_results,
            order="relevance",
            q=name,
            safeSearch="none",
        )
        youtube_response = youtube_request.execute()

        hits = []
        for item in youtube_response["items"]:
            if "id" not in item:
                continue
            if "kind" not in item["id"]:
                continue
            if item["id"]["kind"] != "youtube#video":
                continue
            if "snippet" not in item:
                continue
            if "title" not in item["snippet"]:
                continue
            if len(item["snippet"]["title"].strip()) == 0:
                continue
            title = item["snippet"]["title"]
            video_id = item["id"]["videoId"]
            request = youtube_svc.videos().list(
                part="contentDetails", id=video_id, maxResults=1
            )
            response = request.execute()
            video_duration_string = response["items"][0]["contentDetails"]["duration"]
            video_duration = metomi.isodatetime.parsers.DurationParser().parse(
                video_duration_string
            )
            video_duration_seconds = int(video_duration.get_seconds())
            hits.append(YoutubeSearchHit(video_id, video_duration_seconds, title))

        return hits

    async def get_youtube_link(self, name: str, target_length: float) -> Optional[str]:
        name_hash = hashlib.sha256(
            f"{name} (length {target_length})".encode("utf-8")
        ).hexdigest()
        cache_directory = "cache/yt_search"
        if not os.path.isdir(cache_directory):
            os.makedirs(cache_directory)
        target_filename = f"{cache_directory}/{name_hash}.pickle"

        if not os.path.isfile(target_filename):
            print(
                f"Getting YouTube search results for {name} (length {target_length}) for the first time"
            )

            hits = self.get_youtube_hits_for_query(name, 10)

            best_hit = None
            for hit in hits:
                if best_hit is None:
                    best_hit = hit
                else:
                    absolute_difference_current = abs(target_length - hit.length)
                    absolute_difference_best = abs(target_length - best_hit.length)
                    if absolute_difference_current < absolute_difference_best:
                        best_hit = hit
            if best_hit is None:
                return None
            full_link = f"https://www.youtube.com/watch?v={best_hit.id}"  # type: ignore
            with open(target_filename, "wb") as f:
                pickle.dump(full_link, f)
        else:
            print(
                f"Getting YouTube search results for {name} (length {target_length}) from cache"
            )

        with open(target_filename, "rb") as f2:
            return pickle.load(f2)

    async def get_audio_data_for_youtube_link(
        self,
        youtube_link: str,
    ) -> tuple[bytes, str]:  # TODO get permanent audio file caching
        link_hash = hashlib.sha256(youtube_link.encode("utf-8")).hexdigest()
        cache_directory = "cache/audio"
        if not os.path.isdir(cache_directory):
            os.makedirs(cache_directory)
        target_filename = f"{cache_directory}/{link_hash}.m4a"

        self._logger.info(
            f"Getting audio data for youtube link, {link_hash=} {target_filename=} {youtube_link=}"
        )
        if not os.path.isfile(target_filename):
            self._logger.info(f"Cache miss on {link_hash=} for youtube data")
            try:
                with yt_dlp.YoutubeDL(
                    {
                        "outtmpl": target_filename[:-4],
                        "format": "bestaudio/best",
                        "max_downloads": 1,
                        "postprocessors": [
                            {
                                "key": "FFmpegExtractAudio",
                                "preferredcodec": "aac",
                            }
                        ],
                    }
                ) as youtube_downloader:
                    youtube_downloader.download([youtube_link])
            except yt_dlp.utils.MaxDownloadsReached:
                pass
        else:
            self._logger.info(f"Cache hit on {link_hash=} for youtube data")

        with open(target_filename, "rb") as cached_f:
            cached_b = cached_f.read()
        self._logger.info(
            f"Retrieved {len(cached_b)} bytes from cache on {link_hash=} for youtube data"
        )
        return cached_b, target_filename

    async def get_wav_data_for_youtube_link(
        self,
        youtube_link: str,
    ) -> bytes:
        link_hash = hashlib.sha256(youtube_link.encode("utf-8")).hexdigest()
        cache_directory = "cache/audio"
        target_filename = f"{cache_directory}/{link_hash}.wav"
        self._logger.info(
            f"Getting WAV data for youtube link, {link_hash=} {target_filename=} {youtube_link=}"
        )
        if not os.path.isfile(target_filename):
            print(f"Getting WAV data for {youtube_link} for the first time")
            self._logger.info(f"Cache miss on {link_hash=} for youtube data")
            (
                m4a_bytes,
                _,
            ) = await self.get_audio_data_for_youtube_link(  # dumb_style_checker: disable=word_as_a_digit
                youtube_link
            )
            self._logger.info(f"Got m4a bytes on {link_hash=} for youtube data")
            with BytesIO(m4a_bytes) as f:  # dumb_style_checker: disable=word_as_a_digit
                track = pydub.AudioSegment.from_file(f)
                self._logger.info(f"Parsed m4a bytes on {link_hash=} for youtube data")
                with open(target_filename, "wb") as wav_bytes:
                    track.export(wav_bytes, format="wav")
                    self._logger.info(f"Exported wav on {link_hash=} for youtube data")
        else:
            self._logger.info(f"Cache hit on {link_hash=} for youtube data")
        with open(target_filename, "rb") as cached_f:
            cached_b = cached_f.read()
        self._logger.info(
            f"Retrieved {len(cached_b)} bytes from cache on {link_hash=} for youtube data"
        )
        return cached_b
