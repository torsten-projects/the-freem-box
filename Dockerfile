FROM node:lts AS ts-build
WORKDIR /build
RUN npm install -g typescript
COPY _web/js ./js
RUN tsc ./js/*.ts

FROM jekyll/jekyll:stable as web-build

WORKDIR /build
COPY _web ./_web
COPY --from=ts-build /build/js ./_web/js
RUN chmod -R 777 . && jekyll build --source _web --destination /build/web

FROM ubuntu:22.04 AS backend-build

ENV PYTHONUNBUFFERED=1
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    python3 \
    python3-pip \
	python3-dev \
	gcc \
	musl-dev \
	portaudio19-dev \
	libsndfile1 \
	ffmpeg \
	curl \
	git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV POETRY_VERSION=1.8.5
RUN pip install --no-input "poetry==$POETRY_VERSION"
RUN poetry config virtualenvs.in-project true

WORKDIR /app

COPY poetry.lock pyproject.toml ./

RUN poetry install --no-root --sync

RUN mkdir -p \
	cache/analysis \
	cache/audio \
	cache/driver \
	cache/track \
	cache/yt_search && \
	chmod -R o+rwx cache

COPY --from=web-build /build/web ./web
COPY backend ./backend

ENTRYPOINT [ "poetry", "run", "uvicorn", "backend.uvicorn:app", "--host", "0.0.0.0", "--port", "8080" ]
