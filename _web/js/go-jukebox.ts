declare let Raphael: any;

interface IRaphael {
    _left: any;
    _top: any;
    bottom: any;
    ca: any;
    canvas: any;
    customAttributes: any;
    defs: any;
    desc: any;
    height: any;
    top: any;
    width: any;
    clear(): void;
    path(path: any): any;
    rect(x: any, y: any, w: any, h: any): any;
}

interface Edge { // this is purely visual, no need for typing.. for now
	id: number;
	src: any;
	dest: any;
	distance: number;
	curve: any;
}

interface Curve { // purely visual, no need for typing
    0: any;
    _: any;
    attrs: any;
    edge: Edge;
    id: number;
    matrix: any;
    next: Curve;
    node: any;
    paper: IRaphael;
    prev: Curve;
    realPath: any;
    type: string;
    attr(name: string, value: any);
    toFront();
}

class CompleteTrack {
    analysis: Analysis;
    audio_summary: AnalysisAudioSummary;
    buffer: AudioBuffer;
    fixedTitle: string;
    info: AnalysisTrackInfo;
    status: string;

    constructor(json: string) {
        console.log('CompleteTrack: Parsing analysis JSON');
        let data = JSON.parse(json);
        this.analysis = new Analysis(data['analysis'], this);
        this.audio_summary = data['audio_summary'];
        this.buffer = data['buffer'];
        this.fixedTitle = data['fixedTitle'];
        this.info = data['info'];
        this.status = data['status'];
    }
}

interface AnalysisAudioSummary {
    duration: number;
}

class AnalysisParameters {
    timbreWeight: number;
    pitchWeight: number;
    loudStartWeight: number;
    loudMaxWeight: number;
    durationWeight: number;
    confidenceWeight: number;
    justBackwards: boolean;
    justLongBranches: boolean;
    minLongBranch: number;
    currentThreshold: number;
    addLastEdge: boolean;
    maxBranches: number;
    maxBranchThreshold: number;
}

class AnalysisResult {
    allEdges: Edge[]; // remixer.data.allEdges = allEdges (was truncated inside, so replace instead of append)
    totalBeats: number; // remixer.data.setTotalBeats(quanta.length);
    longestReach: number; // remixer.data.setLongestReach(longestReach);
    lastBranchPoint: number; // remixer.data.lastBranchPoint
    count: number; // original output
}

class Analysis {
    bars: AnalysisBar[];
    beats: AnalysisBeat[];
    sections: AnalysisSection[];
    segments: AnalysisSegment[];
    filtered_segments: AnalysisSegment[];
    tatums: AnalysisBar[];

    constructor(analysis: Analysis, track: CompleteTrack) {
        this.bars = analysis.bars;
        this.beats = analysis.beats;
        this.sections = analysis.sections;
        this.segments = analysis.segments;
        this.filtered_segments = analysis.filtered_segments;
        this.tatums = analysis.tatums;
        
        this.analyzeShallow(track);
    }

    private analyzeShallow(track: CompleteTrack) {
        console.log("Analysis: Performing shallow analysis");
        let types = ['sections', 'bars', 'beats', 'tatums', 'segments'];

        for (let i in types) {
            let type = types[i];
            for (let jString in this[type]) {
                let qlist = this[type];

                let j = parseInt(jString);

                let q = qlist[j];
                q.track = track;
                q.which = j;
                if (j > 0) {
                    q.prev = qlist[j-1];
                } else {
                    q.prev = null
                }

                if (j < qlist.length - 1) {
                    q.next = qlist[j+1];
                } else {
                    q.next = null
                }
            }
        }

        this.connectQuanta('sections', 'bars');
        this.connectQuanta('bars', 'beats');
        this.connectQuanta('beats', 'tatums');
        this.connectQuanta('tatums', 'segments');

        this.connectFirstOverlappingSegment('bars');
        this.connectFirstOverlappingSegment('beats');
        this.connectFirstOverlappingSegment('tatums');

        this.connectAllOverlappingSegments('bars');
        this.connectAllOverlappingSegments('beats');
        this.connectAllOverlappingSegments('tatums');

        this.filterSegments();
    }
    
    analyzeDeep(parameters: AnalysisParameters): AnalysisResult {
        console.log("Analysis: Performing deep analysis", parameters, this.sections);
        let result = new AnalysisResult();
        this.precalculateNearestNeighbors(parameters.maxBranches, parameters.maxBranchThreshold, parameters, result);
        result.count = this.collectNearestNeighbors(parameters.currentThreshold, parameters);
        this.postProcessNearestNeighbors(parameters, result);
        return result;
    }


    private connectQuanta(parent: string, child: string) {
        let last = 0;
        let qparents = this[parent];
        let qchildren = this[child];

        for (let i in qparents) {
            let qparent = qparents[i];
            qparent.children = [];

            for (let j = last; j < qchildren.length; j++) {
                let qchild = qchildren[j];
                if (qchild.start >= qparent.start
                            && qchild.start < qparent.start + qparent.duration) {
                    qchild.parent = qparent;
                    qchild.indexInParent = qparent.children.length;
                    qparent.children.push(qchild);
                    last = j;
                } else if (qchild.start > qparent.start) {
                    break;
                }
            }
        }
    }

    private connectFirstOverlappingSegment(quanta_name: string) {
        let last = 0;
        let quanta = this[quanta_name];
        let segs = this.segments;

        for (let i = 0; i < quanta.length; i++) {
            let q = quanta[i];

            for (let j = last; j < segs.length; j++) {
                let qseg = segs[j];
                if (qseg.start >= q.start) {
                    q.oseg = qseg;
                    last = j;
                    break
                }
            }
        }
    }

    private connectAllOverlappingSegments(quanta_name: string) {
        let last = 0;
        let quanta = this[quanta_name];
        let segs = this.segments;

        for (let i = 0; i < quanta.length; i++) {
            let q = quanta[i];
            q.overlappingSegments = [];

            for (let j = last; j < segs.length; j++) {
                let qseg = segs[j];
                // seg starts before quantum so no
                if ((qseg.start + qseg.duration) < q.start) {
                    continue;
                }
                // seg starts after quantum so no
                if (qseg.start > (q.start + q.duration)) {
                    break;
                }
                last = j;
                q.overlappingSegments.push(qseg);
            }
        }
    }

    private filterSegments() {
        let threshold = .3;
        let fsegs = [];
        fsegs.push(this.segments[0]);
        for (let i = 1; i < this.segments.length; i++) {
            let seg = this.segments[i];
            let last = fsegs[fsegs.length - 1];
            if (JRemixer.isSimilar(seg, last) && seg.confidence < threshold) {
                fsegs[fsegs.length -1].duration += seg.duration;
            } else {
                fsegs.push(seg);
            }
        }
        this.filtered_segments = fsegs;
    }

    private postProcessNearestNeighbors(parameters: AnalysisParameters, result: AnalysisResult) {
        if (parameters.addLastEdge) {
            if (this.longestBackwardBranch() < 50) {
                this.insertBestBackwardBranch(parameters.currentThreshold, 65);
            } else {
                this.insertBestBackwardBranch(parameters.currentThreshold, 55);
            }
        }
        this.calculateReachability();
        let lastBranchPoint = this.findBestLastBeat(result);
        this.filterOutBadBranches(lastBranchPoint);
        result.lastBranchPoint = lastBranchPoint;
    }
    
    private findBestLastBeat(result: AnalysisResult) {
        let reachThreshold = 50;
        let quanta = this.beats;
        let longest = 0;
        let longestReach = 0;
        for (let i = quanta.length - 1; i >= 0; i--) {
            let q = quanta[i];
            let distanceToEnd = quanta.length - i;
    
            // if q is the last quanta, then we can never go past it
            // which limits our reach
            let reach = (q.reach - distanceToEnd) * 100 / quanta.length;
    
            if (reach > longestReach && q.neighbors.length > 0) {
                longestReach = reach;
                longest = i;
                if (reach >= reachThreshold) {
                    break;
                }
            }
        }
    
        result.totalBeats = quanta.length;
        result.longestReach = longestReach;
        return longest;
    }

    private filterOutBadBranches(lastIndex: number) {
        let quanta = this.beats;
        for (let i = 0; i < lastIndex; i++) {
            let q = quanta[i];
            let newList = [];
            for (let j = 0; j < q.neighbors.length; j++) {
                let neighbor = q.neighbors[j];
                if (neighbor.dest.which < lastIndex) {
                    newList.push(neighbor);
                }
            }
            q.neighbors = newList;
        }
    }

    private calculateReachability() {
        let maxIter = 1000;
        let iter = 0;
        let quanta = this.beats;

        for (let qi = 0; qi < quanta.length; qi++) {
            let q = quanta[qi];
            q.reach = quanta.length - q.which;
        }

        for (iter = 0; iter < maxIter; iter++) {
            let changeCount = 0;
            for (let qi = 0; qi < quanta.length; qi++) {
                let q = quanta[qi];
                let changed = false;

                for (let i = 0; i < q.neighbors.length; i++) {
                    let q2 = q.neighbors[i].dest;
                    if (q2.reach > q.reach) {
                        q.reach = q2.reach;
                        changed = true;
                    }
                }

                if (qi < quanta.length - 1) {
                    let q2 = quanta[qi + 1];
                    if (q2.reach > q.reach) {
                        q.reach = q2.reach;
                        changed = true;
                    }
                }

                if (changed) {
                    changeCount++;
                    for (let j = 0; j < q.which; j++) {
                        let q2 = quanta[j];
                        if (q2.reach < q.reach) {
                            q2.reach = q.reach;
                        }
                    }
                }
            }
            if (changeCount == 0) {
                break;
            }
        }
    }

    private longestBackwardBranch() {
        let longest = 0
        let quanta = this.beats;
        for (let i = 0; i < quanta.length; i++) {
            let q = quanta[i];
            for (let j = 0; j < q.neighbors.length; j++) {
                let neighbor = q.neighbors[j];
                let which = neighbor.dest.which;
                let delta = i - which;
                if (delta > longest) {
                    longest = delta;
                }
            }
        }
        let lbb = longest * 100 / quanta.length;
        return lbb;
    }

    private insertBestBackwardBranch(threshold: number, maxThreshold: number) {
        let branches = [];
        let quanta = this.beats;
        for (let i = 0; i < quanta.length; i++) {
            let q = quanta[i];
            for (let j = 0; j < q.all_neighbors.length; j++) {
                let neighbor = q.all_neighbors[j];
    
                let which = neighbor.dest.which;
                let thresh = neighbor.distance;
                let delta = i - which;
                if (delta > 0 && thresh < maxThreshold) {
                    let percent = delta * 100 / quanta.length;
                    let edge = [percent, i, which, q, neighbor]
                    branches.push(edge);
                }
            }
        }
    
        if (branches.length === 0) {
            return;
        }
    
        branches.sort(
            function (a, b) {
                return a[0] - b[0];
            }
        )
        branches.reverse();
        let best = branches[0];
        let bestQ = best[3];
        let bestNeighbor = best[4];
        let bestThreshold = bestNeighbor.distance;
        if (bestThreshold > threshold) {
            bestQ.neighbors.push(bestNeighbor);
        }
    }

    private collectNearestNeighbors(maxThreshold: number, parameters: AnalysisParameters) {
        let branchingCount = 0;
        for (let qi = 0; qi < this.beats.length; qi++) {
            let q1 = this.beats[qi];
            q1.neighbors = this.extractNearestNeighbors(q1, maxThreshold, parameters);
            if (q1.neighbors.length > 0) {
                branchingCount += 1;
            }
        }
        return branchingCount;
    }

    private extractNearestNeighbors(q: AnalysisBeat, maxThreshold: number, parameters: AnalysisParameters) {
        let neighbors = [];
    
        for (let i = 0; i < q.all_neighbors.length; i++) {
            let neighbor = q.all_neighbors[i];
    
            if (parameters.justBackwards && neighbor.dest.which > q.which) {
                continue;
            }
    
            if (parameters.justLongBranches && Math.abs(neighbor.dest.which - q.which) < parameters.minLongBranch) {
                continue;
            }
    
            let distance = neighbor.distance;
            if (distance <= maxThreshold) {
                neighbors.push(neighbor);
            }
        }
        return neighbors;
    }

    private precalculateNearestNeighbors(maxNeighbors: number, maxThreshold: number, parameters: AnalysisParameters, result: AnalysisResult) {
        // skip if this is already done
        if ('all_neighbors' in this.beats[0]) {
            return;
        }
        result.allEdges = [];
        for (let qi = 0; qi < this.beats.length; qi++) {
            let q1 = this.beats[qi];
            this.calculateNearestNeighborsForQuantum(maxNeighbors, maxThreshold, q1, parameters, result);
        }
    }

    private calculateNearestNeighborsForQuantum(maxNeighbors: number, maxThreshold: number, q1: AnalysisBeat, parameters: AnalysisParameters, result: AnalysisResult) {
        let edges = [];
        let id = 0;
        for (let i = 0; i < this.beats.length; i++) {
    
            if (i === q1.which) {
                continue;
            }
    
            let q2 = this.beats[i];
            let sum = 0;
            for (let j = 0; j < q1.overlappingSegments.length; j++) {
                let seg1 = q1.overlappingSegments[j];
                let distance = 100;
                if (j < q2.overlappingSegments.length) {
                    let seg2 = q2.overlappingSegments[j];
                    // some segments can overlap many quantums,
                    // we don't want this self segue, so give them a
                    // high distance
                    if (seg1.which === seg2.which) {
                        distance = 100
                    } else {
                        distance = this.get_seg_distances(seg1, seg2, parameters);
                    }
                }
                sum += distance;
            }
            let pdistance = q1.indexInParent == q2.indexInParent ? 0 : 100;
            let totalDistance = sum / q1.overlappingSegments.length + pdistance;
            if (totalDistance < maxThreshold) {
                let edge: Edge = { // it's fine, no outside type actually used
                    id: id,
                    src: q1,
                    dest: q2,
                    distance: totalDistance,
                    curve: null
                };
                edges.push(edge);
                id++;
            }
        }
    
        edges.sort(
            function (a, b) {
                if (a.distance > b.distance) {
                    return 1;
                } else if (b.distance > a.distance) {
                    return -1;
                } else {
                    return 0;
                }
            }
        );
    
        q1.all_neighbors = [];
        for (let i = 0; i < maxNeighbors && i < edges.length; i++) {
            let edge: Edge = edges[i];
            q1.all_neighbors.push(edge);  
            edge.id = result.allEdges.length;  
            result.allEdges.push(edge);
        }
    }

    private get_seg_distances(seg1: AnalysisSegment, seg2: AnalysisSegment, parameters: AnalysisParameters) {
        let timbre = Tools.seg_distance(seg1, seg2, 'timbre');
        let pitch = Tools.seg_distance(seg1, seg2, 'pitches');
        let sloudStart = Math.abs(seg1.loudness_start - seg2.loudness_start);
        let sloudMax = Math.abs(seg1.loudness_max - seg2.loudness_max);
        let duration = Math.abs(seg1.duration - seg2.duration);
        let confidence = Math.abs(seg1.confidence - seg2.confidence);
        let distance = timbre * parameters.timbreWeight + pitch * parameters.pitchWeight +
            sloudStart * parameters.loudStartWeight + sloudMax * parameters.loudMaxWeight +
            duration * parameters.durationWeight + confidence * parameters.confidenceWeight;
        return distance;
    }
}

interface AnalysisBar {
    confidence: number;
    duration: number;
    start: number;
}

interface AnalysisSection {
    children: AnalysisBeat[];
    confidence: number;
    duration: number;
    key: number;
    key_confidence: number;
    loudness: number;
    mode: number;
    mode_confidence: number;
    next: AnalysisSection;
    prev: AnalysisSection;
    start: number;
    tempo: number;
    tempo_confidence: number;
    time_signature: number;
    time_signature_confidence: number;
    track: CompleteTrack;
    which: number;
}

interface AnalysisSegment {
    confidence: number;
    duration: number;
    loudness_end: number;
    loudness_max: number;
    loudness_max_time: number;
    loudness_start: number;
    pitches: number[];
    start: number;
    timbre: number[];
    next: AnalysisSegment;
    prev: AnalysisSegment;
    track: CompleteTrack;
    which: number;
}

interface AnalysisTrackInfo {
    artist: string;
    duration: number;
    id: string;
    name: string;
    preview_url: string;
    service: string;
    title: string;
    url: string;
}

interface AnalysisBeat {
    all_neighbors: Edge[];
    children: AnalysisBeat[];
    confidence: number;
    duration: number;
    indexInParent: number;
    neighbors: Edge[];
    next: AnalysisBeat;
    oseg: AnalysisSegment;
    overlappingSegments: AnalysisSegment[];
    parent: AnalysisBeat;
    prev: AnalysisBeat;
    reach: number;
    start: number;
    tile: Tile;
    track: CompleteTrack;
    which: number;
    audioSource: AudioBufferSourceNode;
}

class Player {
    context: AudioContext;

    queueTime: number = 0;
    audioGain: GainNode = null;
    curAudioSource: AudioBufferSourceNode = null;
    curQ: AnalysisBeat = null;

    constructor(context: AudioContext) {
        this.context = context;

        this.audioGain = this.context.createGain();
        this.audioGain.gain.value = 0.5;
        this.audioGain.connect(this.context.destination);
    }

    play(when: number, q: AnalysisBeat) {
        console.log('Player: Starting playback');
        return this.playQuantum(when, q);
    }

    queueRest(duration: number) {
        console.log(`Player: Enqueueing rest period (${duration})`);
        this.queueTime += duration;
    }

    stop(q: AnalysisBeat = null) {
        if (q === null) {
            if (this.curAudioSource) {
                console.log('Player: Full stop on global node');
                this.curAudioSource.stop(0);
                this.curAudioSource = null;
            }
        } else {
            if ('audioSource' in q) {
                if (q.audioSource !== null) {
                    console.log('Player: Full stop on local node');
                    q.audioSource.stop(0);
                }
            }
        }
        this.curQ = null;
    }

    curTime() {
        return this.context.currentTime;
    }

    private playQuantum(when: number, q: AnalysisBeat) {
        let now = this.context.currentTime;
        let start = when == 0 ? now : when;
        let timeScalar = remixer.data.playbackRate?.value ?? 1.0;
        let next = start + (q.duration / timeScalar); // adjusting timing for possibly adjusted playback rate
        

        if (this.curQ && this.curQ.track === q.track && this.curQ.which + 1 == q.which) {
            console.log('Player: Stepped');
            // let it ride
        } else {
            console.log('Player: Jumped, enqueueing new buffer');
            let audioSource = this.context.createBufferSource();
            //audioGain.gain.value = 1;
            audioSource.buffer = q.track.buffer;
            
            let originalRate = remixer.data.playbackRate?.value ?? 1.0;
            remixer.data.playbackRate = audioSource.playbackRate;
            remixer.data.playbackRate.value = originalRate;
            audioSource.connect(this.audioGain);
            let duration = remixer.track.audio_summary.duration - q.start;
            audioSource.start(start, q.start, duration);
            if (this.curAudioSource) {
                this.curAudioSource.stop(start);
            }
            this.curAudioSource = audioSource;
        }
        q.audioSource = this.curAudioSource;
        this.curQ = q;
        return next;
    }
}

enum DriverOperation {
    None,
    Next
}

class Driver {
    curTile: Tile = null;
    curOp: DriverOperation = DriverOperation.None;
    incr: number = 1;
    nextTile: Tile = null;
    bounceCount: number = 0;
    nextTime: number = 0;
    startTime: number = 0;
    beatDiv: HTMLElement = document.querySelector("#beats");
    timeDiv: HTMLElement = document.querySelector("#time");
    player: Player = null;

    constructor(player: Player) {
        this.player = player;
    }

    start() {
        console.log('Driver: Starting playback');
        remixer.data.beatsPlayed = 0;
        this.nextTime = 0;
        remixer.data.infiniteMode = true;
        remixer.data.setCurRandomBranchChance(remixer.data.minRandomBranchChance);
        this.curOp = DriverOperation.Next;
        this.startTime = Tools.now();
        document.querySelector("#go").textContent = "Stop";
        UI.error("");
        UI.info("");
        this.resetPlayCounts();
        this.process();
        setTimeout(function () {
            remixer.data.playbackRate.value = remixer.data.bootstrapRateValue;
        }, 200);
        this.player.audioGain.gain.value = remixer.data.initialVolume / 100;
    }

    stop() {
        console.log('Driver: Stopping playback');
        document.querySelector("#go").textContent = "Play";
        if (this.curTile) {
            this.curTile.normal();
            this.curTile = null;
        }
        this.curOp = DriverOperation.None;
        this.incr = 1;
        remixer.data.initialVolume = Math.round(this.player.audioGain.gain.value * 100);
        window.localStorage.setItem('the-freem-box__volume', `${remixer.data.initialVolume}`);
        this.player.stop();
    }

    isRunning() {
        return this.curOp !== DriverOperation.None;
    }

    getIncr() {
        return this.incr;
    }

    getCurTile() {
        return this.curTile;
    }

    setIncr(inc: number) {
        this.incr = inc;
    }

    setNextTile(tile: Tile) {
        this.nextTile = tile;
    }

    private next(): Tile {
        console.log('Driver: Returning next tile');
        if (this.curTile == null || this.curTile == undefined) {
            return remixer.data.tiles[0];
        } else {
            let nextIndex;
            
            nextIndex = this.curTile.which + this.incr;

            if (nextIndex < 0) {
                return remixer.data.tiles[0];
            } else if (nextIndex >= remixer.data.tiles.length) {
                this.curOp = DriverOperation.None;
                this.player.stop();
            } else {
                return this.selectRandomNextTile(remixer.data.tiles[nextIndex]);
            }
        }
    }

    private selectRandomNextTile(seed: Tile) {
        console.log('Driver: Selecting random next tile');
        if (seed.q.neighbors.length == 0) {
            return seed;
        } else if (this.shouldRandomBranch(seed.q)) {
            let next = seed.q.neighbors.shift();
            remixer.data.setLastThreshold(next.distance);
            seed.q.neighbors.push(next);
            let tile = next.dest.tile;
            return tile;
        } else {
            return seed;
        }
    }

    private shouldRandomBranch(q: AnalysisBeat) {
        console.log('Driver: Deciding if we should jump');
        if (remixer.data.infiniteMode) {
            if (q.which == remixer.data.lastBranchPoint) {
                return true;
            }

            remixer.data.setCurRandomBranchChance(remixer.data.curRandomBranchChance + remixer.data.randomBranchChanceDelta);
            if (remixer.data.curRandomBranchChance > remixer.data.maxRandomBranchChance) {
                remixer.data.setCurRandomBranchChance(remixer.data.maxRandomBranchChance);
            }
            let shouldBranch = Math.random() < remixer.data.curRandomBranchChance;
            if (shouldBranch) {
                remixer.data.setCurRandomBranchChance(remixer.data.minRandomBranchChance);
            }
            return shouldBranch;
        } else {
            return false;
        }
    }

    private updateStats() {
        this.beatDiv.textContent = `${remixer.data.beatsPlayed}`;
        this.timeDiv.textContent = Tools.secondsToTime((Tools.now() - this.startTime) / 1000.);
    }

    private process() {
        console.log('Driver: Processing');
        if (this.curTile !== null && this.curTile !== undefined) {
            this.curTile.normal();
        }

        if (this.curOp) {
            let lastTile = this.curTile;
            if (this.nextTile != null) {
                this.curTile = this.nextTile;
                this.nextTile = null;
            } else {
                if(this.curOp == DriverOperation.Next) {
                    this.curTile = this.next();
                }
            }

            if (this.curTile) {
                let ctime = remixer.player.curTime();


                this.nextTime = remixer.player.play(this.nextTime, this.curTile.q);

                this.curTile.playCount += 1;

                let delta = this.nextTime - ctime;
                setTimeout(_ => {
                    this.process();
                }, 1000 * delta - 10);

                let didJump = false;
                if (lastTile && lastTile.which != this.curTile.which - 1) {
                    didJump = true;
                }

                this.curTile.playStyle(didJump);
                remixer.data.beatsPlayed += 1;
                this.updateStats();
            }
        } else {
            if (this.curTile != null) {
                this.curTile.normal();
            }
        }
    }

    private resetPlayCounts() {
        for (let i = 0; i < remixer.data.tiles.length; i++) {
            remixer.data.tiles[i].playCount = 0;
        }
        remixer.data.curGrowFactor = 1;
        UI.redrawTiles();
    }
}

class OriginalGlobalVariables {
	cmin: number[];
	cmax: number[];
    jukeboxData: JukeboxData = new JukeboxData();
    remixer: JRemixer;
    paper: IRaphael;
    track: CompleteTrack;
    curGrowFactor: number = 1;
}

class JukeboxData {
    infiniteMode: boolean = true; // if true, allow branching
    maxBranches: number = 4; // max branches allowed per beat
    maxBranchThreshold: number = 80; // max allowed distance threshold
	minLongBranch: number = 1;

    computedThreshold: number = 0; // computed best threshold
    currentThreshold: number = 30; // current in-use max threshold
    addLastEdge: boolean = true; // if true, optimize by adding a good last edge
    justBackwards: boolean = false; // if true, only add backward branches
    justLongBranches: boolean = false; // if true, only add long branches

    lastBranchPoint: number = 0; // last beat with a good branch
    longestReach: number = 0; // longest looping secstion

    beatsPlayed: number = 0; // total number of beats played
    totalBeats: number = 0; // total number of beats in the song
    branchCount: number = 0; // total number of active branches

    tiles: Tile[] = []; // all of the tiles
    allEdges: Edge[] = []; // all of the edges

    audioURL: string = null; // The URL to play audio from; null means default
    trackID: string = null;

    minRandomBranchChance: number = Constants.defaultMinRandomBranchChance;
    maxRandomBranchChance: number = Constants.defaultMaxRandomBranchChance;
    randomBranchChanceDelta: number = Constants.defaultRandomBranchChanceDelta;
    curRandomBranchChance: number = 0;
    lastThreshold: number = 0;

	playbackRate: AudioParam = null;
	initialVolume: number = Tools.getInitialVolume();
    bootstrapRateValue: number = 1.0;

    curGrowFactor: number = 1;
    cmin: number[];
    cmax: number[];

    setAddLastEdge(value: boolean) {
        this.addLastEdge = value;
        document.querySelector("#last-branch").setAttribute('checked', `${value}`);
        UI.setTunedURL();
    }

    setJustBackwards(value: boolean) {
        this.justBackwards = value;
        document.querySelector("#reverse-branch").setAttribute('checked', `${value}`);
        UI.setTunedURL();
    }

    setJustLongBranches(value: boolean) {
        this.justLongBranches = value;
        document.querySelector("#long-branch").setAttribute('checked', `${value}`);
        UI.setTunedURL();
    }

    setCurrentThreshold(value: number) {
        this.currentThreshold = value;
        document.querySelector("#threshold").textContent = `${value}`;
        (document.querySelector("#threshold-slider") as HTMLInputElement).value = `${value}`;
    }

    setMinRandomBranchChance(value: number) {
        this.minRandomBranchChance = value;
        document.querySelector("#probability-lower").textContent = `${value}`;
        (document.querySelector("#probability-lower-slider") as HTMLInputElement).value = `${value}`;
        this.setCurRandomBranchChance(Tools.clamp(this.curRandomBranchChance,
            this.minRandomBranchChance, this.maxRandomBranchChance));
    }

    setMaxRandomBranchChance(value: number) {
        this.maxRandomBranchChance = value;
        this.setCurRandomBranchChance(Tools.clamp(this.curRandomBranchChance,
            this.minRandomBranchChance, this.maxRandomBranchChance));
    }

    setLastThreshold(value: number) {
        this.lastThreshold = value;
        document.querySelector("#last-threshold").textContent = `${Math.round(value)}`;
    }

    setCurRandomBranchChance(value: number) {
        this.curRandomBranchChance = value;
        document.querySelector("#branch-chance").textContent = `${Math.round(value * 100)}`;
    }

    setRandomBranchChanceDelta(value: number) {
        this.randomBranchChanceDelta = value;
        let val = Math.round(Tools.map_value_to_percent(value,
            Constants.minRandomBranchChanceDelta, Constants.maxRandomBranchChanceDelta));
        document.querySelector("#probability").textContent = `${val}`;
        (document.querySelector("#probability-slider") as HTMLInputElement).value = `${val}`;
    }

    setTotalBeats(value: number) {
        this.totalBeats = value;
        document.querySelector("#total-beats").textContent = `${value}`;
    }

    setBranchCount(value: number) {
        this.branchCount = value;
        document.querySelector("#branch-count").textContent = `${value}`;
    }

    setLongestReach(value: number) {
        this.longestReach = value;
        document.querySelector("#loop-length-percent").textContent = `${Math.round(this.longestReach)}`;
        let loopBeats = Math.round(value * this.totalBeats / 100);
        document.querySelector("#loop-length-beats").textContent = `${Math.round(loopBeats)}`;
        document.querySelector("#total-beats").textContent = `${this.totalBeats}`;
    }

    setAudioURL(value: string) {
        this.audioURL = value;
        (document.querySelector("#audio-url") as HTMLInputElement).value = decodeURIComponent(value);
    }

    setComputedThreshold(value: number) {
        this.computedThreshold = value;
    }

    reset() {
        console.log('JukeboxData: Resetting values');
        this.setAddLastEdge(true);
        this.setJustBackwards(false);
        this.setJustLongBranches(false);
        this.setCurrentThreshold(this.computedThreshold);
        this.setMinRandomBranchChance(Constants.defaultMinRandomBranchChance);
        this.setMaxRandomBranchChance(Constants.defaultMaxRandomBranchChance);
        this.setRandomBranchChanceDelta(Constants.defaultRandomBranchChanceDelta);
        this.setMinRandomBranchChance(Constants.defaultMinRandomBranchChance);
        this.setMaxRandomBranchChance(Constants.defaultMaxRandomBranchChance);
        this.setRandomBranchChanceDelta(Constants.defaultRandomBranchChanceDelta);
        this.setAudioURL(null);
    }
}

class JRemixer {
    context: AudioContext;
    track: CompleteTrack;
    driver: Driver;
    player: Player;
    data: JukeboxData;

    constructor() {
        console.log('JRemixer: Constructing');
        this.context = new AudioContext();
        this.player = new Player(this.context);
        this.data = new JukeboxData();
    }

    trackReady() {
        console.log('JRemixer: Track ready');
        this.track.fixedTitle = this.track.info.name;
        document.title = 'Eternal Jukebox for ' + this.track.fixedTitle;
        document.querySelector("#song-title").textContent = this.track.fixedTitle;
        document.querySelector("#song-url").setAttribute("href", "https://open.spotify.com/track/" + this.track.info.id);
        this.data.minLongBranch = this.track.analysis.beats.length / 5;
    }

    fetchAudio(url: string, context: AudioContext, onDecodedBufferAvailable: Function, is_a_retry: boolean) {
        console.log('JRemixer: Fetching audio file');
        UI.info('Fetching the audio file..');
        let request = new XMLHttpRequest();
        request.onload = _ => {
            if (request.status == 500) {
                if (is_a_retry) {
                    UI.info('Retried fetch failed, the service might be temporarily unavailable');
                } else {
                    UI.info('Initial fetch failed, retrying');
                    setTimeout(() => {
                        this.fetchAudio(url, context, onDecodedBufferAvailable, true);
                    }, 1500);
                }
                return;
            }
            let arrayBufferResponse = request.response;
            context.decodeAudioData(
                arrayBufferResponse,
                decodedBuffer => onDecodedBufferAvailable(decodedBuffer)
            );
        };
        request.open("GET", url, true);
        request.responseType = "arraybuffer";
        request.send();
    }

    fetchAnalysis(id: string) {
        console.log('JRemixer: Fetching analysis JSON');
        let url = '/api/analysis/analyse?spotify_id=' + encodeURIComponent(id);
        UI.info('Fetching the analysis');
    
        let request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.onload = _ => {
            if(request.status == 200) {
                let data: CompleteTrack = new CompleteTrack(request.responseText);
                this.track = data;

                let youtubeURL: string = data.info.url;

                let audioURL: string = "api/audio/youtube?youtube_url=" + encodeURIComponent(youtubeURL);
                this.fetchAudio(audioURL, this.context, buffer => {
                    this.track.buffer = buffer;
                    this.track.status = 'ok';
                    this.driver = new Driver(this.player);
                    UI.readyToPlay();
                    this.trackReady();
                }, false);
            } else {
                UI.info("Sorry, can't find info for that track: HTTP status " + request.status);
            }
        }
        request.send();
    }

    static isSimilar(seg1: AnalysisSegment, seg2: AnalysisSegment) {
        let threshold = 1;
        let distance = Tools.timbral_distance(seg1, seg2);
        return (distance < threshold);
    }

}

abstract class Tools {
    static timbral_distance(s1: AnalysisSegment, s2: AnalysisSegment) {
        return Tools.euclidean_distance(s1.timbre, s2.timbre);
    }

    static secondsToTime(secs: number) {
        secs = Math.floor(secs);
        let hours = Math.floor(secs / 3600);
        secs -= hours * 3600;
        let mins = Math.floor(secs / 60);
        secs -= mins * 60;
    
        let hoursStr = `${hours}`;
        let minsStr = `${mins}`;
        let secsStr = `${secs}`;
    
        if (hours < 10) {
            hoursStr = '0' + hours;
        }
        if (mins < 10) {
            minsStr = '0' + mins;
        }
        if (secs < 10) {
            secsStr = '0' + secs;
        }
        return hoursStr + ":" + minsStr + ":" + secsStr;
    }

    static getInitialVolume(): number {
        return parseInt(window.localStorage.getItem('the-freem-box__volume') || '50', 10);
    }

    static euclidean_distance(v1: number[], v2: number[]) {    
        let sum = 0;
    
        for (let i = 0; i < v1.length; i++) {
            let delta = v2[i] - v1[i];
            sum += delta * delta;
        }
        return Math.sqrt(sum);
    }

    static seg_distance(seg1: AnalysisSegment, seg2: AnalysisSegment, field: string) {
        return Tools.euclidean_distance(seg1[field], seg2[field]);
    }

    static map_percent_to_range(percent: number, min: number, max: number) {
        percent = Tools.clamp(percent, 0, 100);
        return (max - min) * percent / 100. + min;
    }

    static map_value_to_percent(value: number, min: number, max: number) {
        value = Tools.clamp(value, min, max);
        return 100 * (value - min) / (max - min);
    }

    static clamp(val: number, min: number, max: number) {
        return val < min ? min : val > max ? max : val;
    }

    static now() {
        return new Date().getTime();
    }

    static resetTuning() {
        remixer.data.reset();    
        UI.drawVisualization();
    }    

    static noCache() {
        return {
            "noCache": Tools.now()
        }
    }

}

abstract class UI {
    static paper: IRaphael = null;

    static initialize() {
        UI.createRaphael();
        UI.initializeMiscelaneous();
        UI.hookEvents();
    }

    static selectCurve(curve: Curve) { // TODO add as a function to Curve
        curve.attr('stroke-width', 6);
        curve.attr('stroke', Constants.selectColor);
        curve.attr('stroke-opacity', 1.0);
        curve.toFront();
    }

    static readyToPlay() {
        UI.setDisplayMode(true);
        UI.info("Ready!");
        UI.normalizeColor();
        UI.drawVisualization();
    }

    static highlightCurve(curve: Curve, enable: boolean, jump: boolean) { // TODO add as a function to Curve
        if (curve) {
            if (enable) {
                let color = jump ? Constants.jumpHighlightColor : Constants.highlightColor;
                curve.attr('stroke-width', 4);
                curve.attr('stroke', color);
                curve.attr('stroke-opacity', 1.0);
                curve.toFront();
            } else {
                if (curve.edge) {
                    curve.attr('stroke-width', 3);
                    curve.attr('stroke', curve.edge.src.tile.quantumColor);
                    curve.attr('stroke-opacity', .7);
                }
            }
        }
    }

    static processParams() {
        let params = {};
        let q = document.URL.split('?')[1];
        if (q !== undefined) {
            let qSplit = q.split('&');
            for (let i = 0; i < qSplit.length; i++) {
                let pv = qSplit[i].split('=');
                let p = pv[0];
                let v = pv[1];
                params[p] = v;
            }
        }
    
        if ('id' in params) {
            let id = params['id'] as string;
            remixer.data.trackID = id;
    
            if ('thresh' in params) {
                remixer.data.setCurrentThreshold(parseInt(params['thresh'] as string));
            }
            if ('rate' in params) {
                remixer.data.bootstrapRateValue = parseFloat(params['rate'] as string);
                (document.querySelector("#speed-slider") as HTMLInputElement).value = `${remixer.data.bootstrapRateValue * 100}`;
            }
            if ('audio' in params) {
                remixer.data.setAudioURL(decodeURIComponent(params['audio'] as string));
            }
            if ('lb' in params) {
                if (params['lb'] === '0') {
                    remixer.data.setAddLastEdge(true);
                }
            }
    
            if ('jb' in params) {
                if (params['jb'] === '1') {
                    remixer.data.setJustBackwards(true);
                }
            }
    
            if ('lg' in params) {
                if (params['lg'] === '1') {
                    remixer.data.setJustLongBranches(true);
                }
            }
    
            if ('bp' in params) {
                let bp = params['bp'] as string;
                let fields = bp.split(',');
                if (fields.length === 3) {
                    let minRange = parseInt(fields[0]);
                    let maxRange = parseInt(fields[1]);
                    let delta = parseInt(fields[2]);
    
                    remixer.data.setMinRandomBranchChance(Tools.map_percent_to_range(minRange, 0, 1));
                    remixer.data.setMaxRandomBranchChance(Tools.map_percent_to_range(maxRange, 0, 1));
                    remixer.data.setRandomBranchChanceDelta(Tools.map_percent_to_range(delta, Constants.minRandomBranchChanceDelta, Constants.maxRandomBranchChanceDelta));
    
                }
            }
            UI.setDisplayMode(true);
            remixer.fetchAnalysis(id);
        } else {
            UI.setDisplayMode(false);
        }
    }

    static removeAllTiles() {
        for (let i = 0; i < remixer.data.tiles.length; i++) {
            remixer.data.tiles[i].rect.remove();
        }
        remixer.data.tiles = [];
    }

    static to_rgb(r: number, g: number, b: number) {
        return "#" + UI.convert(r * 255) + UI.convert(g * 255) + UI.convert(b * 255);
    }

    static convert(value: number) {
        let integer = Math.round(value);
        let str = Number(integer).toString(16);
        return str.length === 1 ? "0" + str : str;
    };

    static getSegmentColor(seg: AnalysisSegment) {
        let results = [];
        for (let i = 0; i < 3; i++) {
            let t = seg.timbre[i + 1];
            let norm = (t - remixer.data.cmin[i]) / (remixer.data.cmax[i] - remixer.data.cmin[i]);
            results[i] = norm * 255;
            results[i] = norm;
        }
        return UI.to_rgb(results[1], results[2], results[0]);
    }

    static normalizeColor() {
        remixer.data.cmin = [100, 100, 100];
        remixer.data.cmax = [-100, -100, -100];
    
        let qlist = remixer.track.analysis.segments;
        for (let i = 0; i < qlist.length; i++) {
            for (let j = 0; j < 3; j++) {
                let t = qlist[i].timbre[j + 1];
    
                if (t < remixer.data.cmin[j]) {
                    remixer.data.cmin[j] = t;
                }
                if (t > remixer.data.cmax[j]) {
                    remixer.data.cmax[j] = t;
                }
            }
        }
    }

    static createTilePanel(which: string) {
        UI.removeAllTiles();
        remixer.data.tiles = UI.createTiles(which);
    }

    static createNewTile(which: number, q: AnalysisBeat, height: number, width: number): Tile {
        let tile = new Tile();
        tile.which = which;
        tile.width = width;
        tile.height = height;
        tile.branchColor = UI.getBranchColor(q);
        tile.quantumColor = UI.getQuantumColor(q);
        tile.normalColor = tile.quantumColor;
        tile.isPlaying = false;
        tile.isScaled = false;
        tile.playCount = 0;
    
        tile.rect = UI.paper.rect(0, 0, tile.width, tile.height);
        tile.rect.attr("stroke", tile.normalColor);
        tile.rect.attr('stroke-width', 0);
        tile.q = q;
        tile.init();
        q.tile = tile;
        tile.normal();
        return tile;
    }

    static getBranchColor(q: AnalysisBeat) {
        if (q.neighbors.length === 0) {
            return UI.to_rgb(0, 0, 0);
        } else {
            let red = q.neighbors.length / remixer.data.maxBranches;
            return UI.to_rgb(red, 0, (1. - red));
        }
    }

    static getQuantumSegment(q: AnalysisBeat) {
        return q.oseg;
    }

    static getQuantumColor(q: AnalysisBeat) {
        let segment = UI.getQuantumSegment(q);
        if (segment != null) {
            return UI.getSegmentColor(segment);
        } else {
            return "#000";
        }
    }

    static highlightCurves(tile: Tile, enable: boolean, didJump: boolean) {
        for (let i = 0; i < tile.q.neighbors.length; i++) {
            let curve = tile.q.neighbors[i].curve;
            UI.highlightCurve(curve, enable, didJump);
            if (remixer.driver.isRunning()) {
                break; // just highlight the first one
            }
        }
    }

    static redrawTiles() {
        remixer.data.tiles.forEach(tile => {
            let newWidth = Math.round((Constants.minTileWidth + tile.playCount * Constants.growthPerPlay) * remixer.data.curGrowFactor);
            if (newWidth < 1) {
                newWidth = 1;
            }
            tile.rect.attr('width', newWidth);
        });
    }

    static setTunedURL() {
        if (remixer.track) {
            let addBranchParams = false;
            let lb = '';
    
            if (!remixer.data.addLastEdge) {
                lb = '&lb=0';
            }

            let old_id = (new URL(document.location.toString())).searchParams.get("id");
            let p = `?id=${old_id}${lb}`;
    
            if (remixer.data.justBackwards) {
                p += '&jb=1'
            }
    
            if (remixer.data.justLongBranches) {
                p += '&lg=1'
            }
    
            if (remixer.data.currentThreshold !== remixer.data.computedThreshold) {
                p += '&thresh=' + remixer.data.currentThreshold;
            }
    
            if (remixer.data.playbackRate) {
                p += '&rate=' + remixer.data.playbackRate.value;
            }
    
            if (remixer.data.audioURL !== null) {
                p += "&audio=" + encodeURIComponent(remixer.data.audioURL);
            }
    
            if (remixer.data.minRandomBranchChance !== Constants.defaultMinRandomBranchChance) {
                addBranchParams = true;
            }
            if (remixer.data.maxRandomBranchChance != Constants.defaultMaxRandomBranchChance) {
                addBranchParams = true;
            }
    
            if (remixer.data.randomBranchChanceDelta != Constants.defaultRandomBranchChanceDelta) {
                addBranchParams = true;
            }
    
            if (addBranchParams) {
                p += '&bp=' + [
                    Math.round(Tools.map_value_to_percent(remixer.data.minRandomBranchChance, 0, 1)),
                    Math.round(Tools.map_value_to_percent(remixer.data.maxRandomBranchChance, 0, 1)),
                    Math.round(Tools.map_value_to_percent(remixer.data.randomBranchChanceDelta,
                        Constants.minRandomBranchChanceDelta, Constants.maxRandomBranchChanceDelta))
                ].join(',')
            }
    
            history.replaceState({}, document.title, p);
        }
    }

    static drawVisualization() {
        console.log('UI: Drawing visualization');
        if (remixer.track) {
            let parameters: AnalysisParameters = {
                timbreWeight: Constants.timbreWeight,
                pitchWeight: Constants.pitchWeight,
                loudStartWeight: Constants.loudStartWeight,
                loudMaxWeight: Constants.loudMaxWeight,
                durationWeight: Constants.durationWeight,
                confidenceWeight: Constants.confidenceWeight,
                justBackwards: remixer.data.justBackwards,
                justLongBranches: remixer.data.justLongBranches,
                minLongBranch: remixer.data.minLongBranch,
                currentThreshold: remixer.data.currentThreshold,
                addLastEdge: remixer.data.addLastEdge,
                maxBranches: remixer.data.maxBranches,
                maxBranchThreshold: remixer.data.maxBranchThreshold,
            };
            let result: AnalysisResult = remixer.track.analysis.analyzeDeep(parameters);
            remixer.data.allEdges = result.allEdges;
            remixer.data.setTotalBeats(result.totalBeats);
            remixer.data.setLongestReach(result.longestReach);
            remixer.data.lastBranchPoint = result.lastBranchPoint;

            UI.setTunedURL();
            UI.createTilePanel('beats');
        }
    }
    
    static extractTitle(url: string) {
        let lastSlash = url.lastIndexOf('/');
        if (lastSlash >= 0 && lastSlash < url.length - 1) {
            let res = url.substring(lastSlash + 1, url.length - 4);
            return res;
        } else {
            return url;
        }
    }

    
    static createTileCircle(qtype: string, radius: number) {
        console.log('UI: Creating tile circle');
        let y_padding = 90;
        let x_padding = 200;
        let maxWidth = 90;
        let tiles: Tile[] = [];
        let qlist = remixer.track.analysis[qtype];
        let n = qlist.length;
        let R = radius;
        let alpha = Math.PI * 2 / n;
        let perimeter = 2 * n * R * Math.sin(alpha / 2);
        let a = perimeter / n;
        let width = a * 20;
        let angleOffset = -Math.PI / 2;
    
        if (width > maxWidth) {
            width = maxWidth;
        }
    
        width = Constants.minTileWidth;
    
        UI.paper.clear();
    
        let angle = angleOffset;
        for (let i = 0; i < qlist.length; i++) {
            let tile = UI.createNewTile(i, qlist[i], a, width);
            let y = y_padding + R + R * Math.sin(angle);
            let x = x_padding + R + R * Math.cos(angle);
            tile.move(x, y);
            tile.rotate(angle);
            tiles.push(tile);
            angle += alpha;
        }
    
        // now connect every tile to its neighbors
    
        // a horrible hack until I figure out
        // geometry
        let roffset = width / 2;
        let yoffset = width * .52;
        let xoffset = width * 1;
        let center = ' S 450 350 ';
        let branchCount = 0;
        R -= roffset;
        for (let i = 0; i < tiles.length; i++) {
            let startAngle = alpha * i + angleOffset;
            let tile = tiles[i];
            let y1 = y_padding + R + R * Math.sin(startAngle) + yoffset;
            let x1 = x_padding + R + R * Math.cos(startAngle) + xoffset;
    
            for (let j = 0; j < tile.q.neighbors.length; j++) {
                let destAngle = alpha * tile.q.neighbors[j].dest.which + angleOffset;
                let y2 = y_padding + R + R * Math.sin(destAngle) + yoffset;
                let x2 = x_padding + R + R * Math.cos(destAngle) + xoffset;
    
                let path = 'M' + x1 + ' ' + y1 + center + x2 + ' ' + y2;
                let curve = UI.paper.path(path);
                curve.edge = tile.q.neighbors[j];
                UI.highlightCurve(curve, false, false);
                tile.q.neighbors[j].curve = curve;
                branchCount++;
            }
        }
        remixer.data.setBranchCount(branchCount);
        return tiles;
    }

    static createTiles(qtype: string) { // TODO change to enum
        return UI.createTileCircle(qtype, 250);
    }

    static hideAll() {
        ([
            document.querySelector("#song-div"),
            document.querySelector("#select-track"),
            document.querySelector("#running"),
            document.querySelector(".rotate")
        ] as HTMLElement[]).forEach(element => {
            element.style.display = "none";
        });
    }

    static setDisplayMode(playMode: boolean) {
        ([
            document.querySelector("#song-div"),
            document.querySelector("#select-track"),
            document.querySelector(".rotate")
        ] as HTMLElement[]).forEach(element => {
            if(element !== null)
                element.style.display = playMode ? "none" : "block";
        });
        
        const elementRunning = document.querySelector("#running") as HTMLElement;
        if(elementRunning !== null)
            elementRunning.style.display = playMode ? "block" : "none";
    
            UI.info("");
    }

    static error(s: string) {
        const elementError = document.querySelector("#error") as HTMLElement;
        if (s.length == 0) {
            elementError.style.display = "none";
        } else {
            elementError.textContent = s;
            elementError.style.display = "block";
        }
    }
    
    static info(s: string) {
        document.querySelector("#info").textContent = s;
    }

    private static hookEvents() {
        console.log('UI: Hooking events');
        document.ondblclick = event => {
            event.preventDefault();
            event.stopPropagation();
            return false;
        };

        document.querySelector("#go").addEventListener("click", _ => {
            if (remixer.driver.isRunning()) {
                remixer.driver.stop();
            } else {
                remixer.driver.start();
            }
        });

        document.querySelector("#reset-edges").addEventListener("click", _ => {
            Tools.resetTuning();
        });

        document.querySelector("#last-branch").addEventListener("change", _ => {
            remixer.data.addLastEdge = document.querySelector('#last-branch').hasAttribute('checked');
            UI.drawVisualization();
        });

        document.querySelector("#reverse-branch").addEventListener("change", event => {
            remixer.data.setJustBackwards(document.querySelector('#reverse-branch').hasAttribute('checked'));
            UI.drawVisualization();
        });
    
        document.querySelector("#long-branch").addEventListener("change", event => {
            remixer.data.setJustLongBranches(document.querySelector('#long-branch').hasAttribute('checked'));
            UI.drawVisualization();
        });
    
        document.querySelector("#threshold-slider").addEventListener("input", function(event) {
            remixer.data.setCurrentThreshold((event.target as HTMLInputElement).valueAsNumber);
            UI.drawVisualization();
        });
    
        document.querySelector("#probability-lower-slider").addEventListener("input", function (event) {
            let value = (event.target as HTMLInputElement).valueAsNumber;
            remixer.data.minRandomBranchChance = value / 100.;
            document.querySelector("#probability-lower").textContent = `${value}`;
            UI.setTunedURL();
        });
    
        document.querySelector("#probability-higher-slider").addEventListener("input", function (event) {
            let value = (event.target as HTMLInputElement).valueAsNumber;
            remixer.data.maxRandomBranchChance = value / 100.;
            document.querySelector("#probability-higher").textContent = `${value}`;
            UI.setTunedURL();
        });
    
        document.querySelector("#probability-slider").addEventListener("input", function (event) {
            let value = (event.target as HTMLInputElement).valueAsNumber;
            remixer.data.randomBranchChanceDelta =
            Tools.map_percent_to_range(value, Constants.minRandomBranchChanceDelta, Constants.maxRandomBranchChanceDelta);
            document.querySelector("#probability").textContent = `${value}`;
            UI.setTunedURL();
        });
    
        document.querySelector("#audio-url").addEventListener("keyup", event => {
            let keyboardEvent = event as KeyboardEvent;
            let keycode = keyboardEvent.key;
            if (keycode == 'Enter') {
                remixer.data.audioURL = (event.target as HTMLInputElement).value;
                UI.setTunedURL();
                window.location.reload();
            }
        });
        
        document.querySelector("#volume-slider").addEventListener("input", event => {
            let volume = (event.target as HTMLInputElement).valueAsNumber;
            document.querySelector("#volume").textContent = `${volume}`;
            remixer.player.audioGain.gain.value = volume / 100.;
            window.localStorage.setItem('the-freem-box__volume', `${volume}`);
        });
    
        document.querySelector("#speed-slider").addEventListener("input", function(event) {
            let speed = (event.target as HTMLInputElement).valueAsNumber;
            document.querySelector("#speed").textContent = `${speed}`;
            let newRate = speed / 100.0;
            if (remixer.data.playbackRate) {
                remixer.data.playbackRate.value = newRate;
            }
        });
    }

    private static createRaphael() {
        UI.paper = Raphael("tiles", Constants.W, Constants.H);
    }

    private static initializeMiscelaneous() {
        (document.querySelector("#error") as HTMLElement).hidden = true;
        let initialVolume = Tools.getInitialVolume();
        document.querySelector("#volume").textContent = `${initialVolume}`;
        (document.querySelector("#volume-slider") as HTMLInputElement).value = `${initialVolume}`;
    }
}

class Tile {
    branchColor: string;
    height: number;
    isPlaying: boolean;
    isScaled: boolean;
    normalColor: string = "#5f9";
    playCount: number;
    q: AnalysisBeat;
    quantumColor: string;
    rect: any; // just visual, ignore
    which: number;
    width: number;
    label: any; // just visual

    move(x: number, y: number) {
        this.rect.attr({
            x: x,
            y: y
        });
        if (this.label) {
            this.label.attr({
                x: x + 2,
                y: y + 8
            });
        }
    }

    rotate(angle: number) {
        let dangle = 360 * (angle / (Math.PI * 2));
        this.rect.transform('r' + dangle);
    }

    play(force: boolean) {
        if (force) {
            this.playStyle(true);
            remixer.player.play(0, this.q);
            UI.info("Selected tile " + this.q.which);
        } else {
            this.selectStyle();
        }
    }

    selectStyle() {
        this.rect.attr("fill", "#C9a");
    }

    queueStyle() {
        this.rect.attr("fill", "#aFF");
    }

    pauseStyle() {
        this.rect.attr("fill", "#F8F");
    }

    playStyle(didJump: boolean) {
        if (!this.isPlaying) {
            this.isPlaying = true;
            if (!this.isScaled) {
                this.isScaled = true;
                this.rect.attr('width', Constants.maxTileWidth);
            }
            this.rect.toFront();
            this.rect.attr("fill", Constants.highlightColor);
            UI.highlightCurves(this, true, didJump);
        }
    }


    normal() {
        this.rect.attr("fill", this.normalColor);
        if (this.isScaled) {
            this.isScaled = false;
            //this.rect.scale(1/1.5, 1/1.5);
            let newWidth = Math.round((Constants.minTileWidth + this.playCount * Constants.growthPerPlay) * remixer.data.curGrowFactor);
            if (newWidth < 1) {
                newWidth = 1;
            }
            if (newWidth > 90) {
                remixer.data.curGrowFactor /= 2;
                UI.redrawTiles();
            } else {
                this.rect.attr('width', newWidth);
            }
        }
        UI.highlightCurves(this, false, false);
        this.isPlaying = false;
    }

    init() {
        let that = this;

        this.rect.mouseover(function (event: MouseEvent) {
            that.playStyle(false);
            if (Constants.debugMode) {
                if (that.q.which > remixer.data.lastBranchPoint) {
                    document.querySelector("#beats").textContent = that.q.which + ' ' + that.q.reach + '*';
                } else {
                    let qlength = remixer.track.analysis.beats.length;
                    let distanceToEnd = qlength - that.q.which;
                    document.querySelector("#beats").textContent = that.q.which + ' ' + that.q.reach +
                        ' ' + Math.floor((that.q.reach - distanceToEnd) * 100 / qlength);
                }
            }
            event.preventDefault();
        });

        this.rect.mouseout(function (event: MouseEvent) {
            that.normal();
            event.preventDefault();
        });

        this.rect.mousedown(function (event: MouseEvent) {
            event.preventDefault();
            remixer.driver.setNextTile(that);
            if (!remixer.driver.isRunning()) {
                remixer.driver.start();
            }
        });
    }
}

abstract class Constants {
    static readonly W: number = 900;
    static readonly H: number = 680;
    static readonly defaultMinRandomBranchChance: number = .18
    static readonly defaultMaxRandomBranchChance: number = .5
    static readonly defaultRandomBranchChanceDelta: number = .018;
    static readonly minRandomBranchChanceDelta: number = .000;
    static readonly maxRandomBranchChanceDelta: number = .200;
    static readonly minTileWidth: number = 10;
    static readonly maxTileWidth: number = 90;
    static readonly growthPerPlay: number = 10;
    static readonly timbreWeight: number = 1;
    static readonly pitchWeight: number = 10;
    static readonly loudStartWeight: number = 1;
    static readonly loudMaxWeight: number = 1;
    static readonly durationWeight: number = 100;
    static readonly confidenceWeight: number = 1;

    static readonly uploadingAllowed: boolean = false;
    static readonly debugMode: boolean = true;

    static readonly highlightColor: string = "#0000ff";
    static readonly jumpHighlightColor: string = "#00ff22";
    static readonly selectColor: string = "#ff0000";    
}

// This code will make you cry. It was written in a mad
// dash during Music Hack Day Boston 2012, and has
// quite a bit of hackage of the bad kind in it.

let remixer: JRemixer;

function init() {
    console.log('Global: Window ready');
    UI.initialize();
    remixer = new JRemixer();
    UI.processParams();
}

window.onload = init;
